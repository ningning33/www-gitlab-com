---
layout: handbook-page-toc
title: "Open Source Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Open Source Program Overview
*At GitLab, our mission is to change all creative work from read-only to read-write so that everyone can contribute.* The Open Source Program exists to support communities that are also pursuing this goal by offering them our top tiers for free. 

Qualifying Open Source projects can start benefiting from self-hosted Ultimate and cloud-hosted Gold by completing a simple [application process](https://about.gitlab.com/solutions/open-source/program/).

### What's included?
- GitLab's top tiers (self-hosted Ultimate and cloud-hosted Gold) are [free for OSS projects](/solutions/open-source/)

### Who qualifies for the OSS program?

- Any project that uses an OSI-approved open source license and which does not seek to make a profit from the resulting project software may apply.
- See our [full terms](/terms/#edu-oss) regarding this program
- For more questions, please see the [FAQ section](/solutions/open-source/#FAQ)

## Deep Dive into the OSS program
If you're interested in learning more about how the OSS program operates, or if you are working on it directly and are looking for more information, please read on. 

## Where to find what we are working on
### Goals for current quarter
GitLab creates [Objectives and Key Results (OKRs)](https://en.wikipedia.org/wiki/OKR) per quarter. Here are the current OKRs for the Open Source Program:
 * [Revamp OSS Program to support growth](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/10)
 * [Build up OSS program relationships](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/11)

### General workspaces
 * [Open Source Program project board](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/general/-/boards) - check this out for a general overview of internal work happening to support our OSS Program
 * [Issues labeled "Open Source"](https://gitlab.com/groups/gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Open%20Source) - search the label on GitLab.com to include public issues filed by OSS projects

## Workflows and resources

- Community Advocacy [workflow](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/)
- GitLab OSS program [repository](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/gitlab-oss/)

## Metrics

SFDC Opportunities by Stage for OSS campaign - SFDC [report](https://na34.salesforce.com/00O61000004hfom)
