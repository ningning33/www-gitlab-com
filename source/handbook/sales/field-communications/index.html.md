---
layout: handbook-page-toc
title: "Field Communications"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Welcome to the Field Communications Handbook

This team is responsible for creating effective, timely, and easily consumable communications with Sales and Customer Success audiences. Our goal is to help the field sell better, faster, and smarter with communication programs that keep them better informed of organizational/business updates that impact their roles, as well as useful resources that will enable day-to-day work. 

This page is a work in progress, (WIP) and we will update it as we add more Field Communications-related content to the Handbook. 

For more information about the Field Enablement team, visit the [team handbook page.](/handbook/sales/field-operations/field-enablement/)
